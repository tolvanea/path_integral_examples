import numpy as np
import typing as tp
from numba import njit
import scipy

# Todo reuse code on differing dimensions.½

@njit
def rnd_walker_1d(box_side_size: int,
                  box_side_len: float,
                  num_walkers: int,
                  num_samples: int,
                  interval: int,
                  step_size: float):
    dim = 1
    box_dims = (box_side_size,)
    a = box_side_len / 2.0
    box_lower_corner = np.array([-a for i in range(dim)])
    box_upper_corner = np.array([a for i in range(dim)])

    dens_int = np.zeros(box_dims, dtype=np.int_)

    for walker in range(num_walkers):
        r: np.ndarray = np.zeros(dim)
        for i in range(num_samples):
            step = (np.random.rand(interval, dim) - 0.5) * step_size
            for j in range(interval):
                r += step[j, :]

            ids = position_into_dens_index_1d(r,
                                              box_lower_corner,
                                              box_upper_corner,
                                              box_dims)
            if ids is not None:
                (x,) = ids
                dens_int[x] += 1


    # normalize
    count = float(num_walkers*num_samples)
    dens = dens_int / count

    return dens

@njit
def rnd_walker_2d(box_side_size: int,
                  box_side_len: float,
                  num_walkers: int,
                  num_samples: int,
                  interval: int,
                  step_size: float):
    dim = 2
    box_dims = (box_side_size, box_side_size)
    a = box_side_len / 2.0
    box_lower_corner = np.array([-a for i in range(dim)])
    box_upper_corner = np.array([a for i in range(dim)])

    dens_int = np.zeros(box_dims, dtype=np.int_)

    for walker in range(num_walkers):
        r: np.ndarray = np.zeros(dim)
        for i in range(num_samples):
            step = (np.random.rand(interval, dim) - 0.5) * step_size
            for j in range(interval):
                r += step[j, :]

            ids = position_into_dens_index_2d(r,
                                              box_lower_corner,
                                              box_upper_corner,
                                              box_dims)
            if ids is not None:
                x, y = ids
                dens_int[x, y] += 1


    # normalize
    count = float(num_walkers*num_samples)
    dens = dens_int / count

    return dens


@njit
def rnd_walker_2d_path(box_side_size: int,
                  box_side_len: float,
                  num_walkers: int,
                  num_samples: int,
                  interval: int,
                  step_size: float):
    """
    This function only exists for plotting two images to slides.
    """
    dim = 2
    box_dims = (box_side_size, box_side_size)
    a = box_side_len / 2.0
    box_lower_corner = np.array([-a for i in range(dim)])
    box_upper_corner = np.array([a for i in range(dim)])

    path = np.empty((num_samples,2))

    for walker in range(1):
        r: np.ndarray = np.zeros(dim)
        for i in range(num_samples):
            step = (np.random.rand(interval, dim) - 0.5) * step_size
            for j in range(interval):
                r += step[j, :]

            path[i,:] = r

    return path


@njit
def rnd_walker_3d(box_side_size: int,
                 box_side_len: float,
                 num_walkers: int,
                 num_samples: int,
                 interval: int,
                 step_size: float) -> np.ndarray:
    box_dims = (box_side_size, box_side_size, box_side_size);
    a = box_side_len / 2.0;
    box_lower_corner = np.array([-a, -a, -a])
    box_upper_corner = np.array([a, a, a])

    dens_int = np.zeros(box_dims, dtype=np.int_)

    for walker in range(num_walkers):
        r: np.ndarray = np.array([0.0, 0.0, 0.0])
        for i in range(num_samples):
            step = (np.random.rand(interval, 3) - 0.5) * step_size
            for j in range(interval):
                r += step[j, :]

            ids = position_into_dens_index_3d(r,
                                              box_lower_corner,
                                              box_upper_corner,
                                              box_dims)
            if ids is not None:
                x, y, z = ids
                dens_int[x, y, z] += 1

    # normalize
    count = float(num_walkers*num_samples)
    dens = dens_int / count

    return dens

Point = np.ndarray

@njit
def position_into_dens_index_1d(r: Point,
                                low: Point,
                                upp: Point,
                                ns: tp.Tuple[int]) \
        -> tp.Union[tp.Tuple[int], None]:
    (nx,) = ns
    x_i = int(np.floor((r[0] - low[0]) * (nx) / (upp[0] - low[0])))

    if 0 <= x_i and x_i < nx:
        return (x_i,)
    else:
        return None


@njit
def position_into_dens_index_2d(r: Point,
                                low: Point,
                                upp: Point,
                                ns: tp.Tuple[int, int]) \
        -> tp.Union[tp.Tuple[int, int], None]:
    nx, ny = ns
    x_i = int(np.floor((r[0] - low[0]) * (nx) / (upp[0] - low[0])))
    y_i = int(np.floor((r[1] - low[1]) * (ny) / (upp[1] - low[1])))

    if (0 <= x_i and x_i < nx) \
            and (0 <= y_i and y_i < ny):
        return (x_i, y_i)
    else:
        return None


@njit
def position_into_dens_index_3d(r: Point,
                             low: Point,
                             upp: Point,
                             ns: tp.Tuple[int, int, int]) \
                             -> tp.Union[tp.Tuple[int, int, int], None]:
    nx, ny, nz = ns
    x_i = int(np.floor((r[0] - low[0]) * (nx) / (upp[0] - low[0])))
    y_i = int(np.floor((r[1] - low[1]) * (ny) / (upp[1] - low[1])))
    z_i = int(np.floor((r[2] - low[2]) * (nz) / (upp[2] - low[2])))

    if (0 <= x_i and x_i < nx) \
            and (0 <= y_i and y_i < ny) \
            and (0 <= z_i and z_i < nz):
        return (x_i, y_i, z_i)
    else:
        return None

def main():
    box_side_size = 65
    box_side_len = 2.0
    num_walkers = 100
    num_samples = 10000
    interval = 100
    step_size = 0.001
    rnd_walker_3d(box_side_size,
                   box_side_len,
                   num_walkers,
                   num_samples,
                   interval,
                   step_size)

if __name__ == "__main__":
    main()
