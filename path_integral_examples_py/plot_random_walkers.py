import numpy as np
from rnd_walker_py import rnd_walker_2d_path, rnd_walker_2d
import matplotlib.pyplot as plt
import time


def main():
    # slide of brownian motion
    plot_rnd_walker_and_paths_2d()
    plt.show()

def plot_rnd_walker_and_paths_2d():
    """
    Plot images for slide about brownian motion.
    """
    box_side_size = 65
    box_side_len = 2.0
    num_walkers = 1000
    num_samples = 5000
    interval = 100
    step_size = 0.003

    # 2d random walker path
    path = rnd_walker_2d_path(box_side_size,
                                    box_side_len,
                                    num_walkers,
                                    num_samples,
                                    interval,
                                    step_size)
    fig1, (ax1) = plt.subplots(1,1, figsize=(5,5))
    ax1.set_ylim(-box_side_len//2,box_side_len//2)
    ax1.set_xlim(-box_side_len//2,box_side_len//2)
    ax1.plot(path[::100,0],path[::100,1])
    fig2, (ax2) = plt.subplots(1,1, figsize=(5,5))
    ax2.plot(path[:,0], path[:,1])
    ax2.set_ylim(-box_side_len//2,box_side_len//2)
    ax2.set_xlim(-box_side_len//2,box_side_len//2)

    # 2d random walker density
    arr = rnd_walker_2d(box_side_size,
                        box_side_len,
                        num_walkers*interval,
                        1,  # num_samples/,
                        num_samples,
                        step_size*10) # This is false scaling, really it should
                                      # be *10, but the figure is not as nice
                                      # then. (X and y axis are not scaled with
                                      # path coordinates.)
    fig3, (ax3) = plt.subplots(1,1, figsize=(5,4))
    ax3.imshow(arr)

    ax1.axes.get_xaxis().set_visible(False)
    ax1.axes.get_yaxis().set_visible(False)
    ax2.axes.get_xaxis().set_visible(False)
    ax2.axes.get_yaxis().set_visible(False)
    ax3.axes.get_xaxis().set_visible(False)
    ax3.axes.get_yaxis().set_visible(False)

    ax1.plot([0], [0], marker="x", c="r", markersize=10)
    ax2.plot([0], [0], marker="x", c="r", markersize=10)
    ax1.plot(path[-1,0], path[-1,1], marker="x", c="r", markersize=10)
    ax2.plot(path[-1,0], path[-1,1], marker="x", c="r", markersize=10)
    ax3.plot([box_side_size//2], [box_side_size//2], marker="x", c="r",
             zorder=20, markersize=10)

    fig1.savefig("images_in_slides/path_coarce.pdf")
    fig1.savefig("images_in_slides/path_coarce.png")
    fig2.savefig("images_in_slides/path_fine.pdf")
    fig2.savefig("images_in_slides/path_fine.png")
    fig3.savefig("images_in_slides/path_density.pdf")
    fig3.savefig("images_in_slides/path_density.png")


if __name__ == "__main__":
    main()
