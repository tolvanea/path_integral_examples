import numpy as np
import matplotlib.pyplot as plt
import naive_kernel
from scipy.stats import norm

def test_draw_sample_from_cumulative_distribution():
    """Draw plots to see by eye does sample generator work."""

    xax = np.linspace(1,3,50)
    # pdf = np.random.normal(loc=1.5, scale=0.5, size=50)
    pdf = norm.pdf(xax, loc=1.5, scale=0.5)
    cdf = np.cumsum(pdf) * (2/50)
    pdf /= cdf[-1]

    rnd = np.zeros(50)
    for i in range(100000):
        x, idx = naive_kernel.draw_sample_from_cumulative_distribution(1.0,3.0,
                                                                       cdf)
        id = round((x-1)/2.0 * 50)
        #print(idx, id)
        #assert idx == id
        rnd[id] += 1/100000 * 50 / 2

    plt.plot(xax, pdf)
    plt.plot(xax, cdf)
    plt.plot(xax, rnd)

    plt.show()

test_draw_sample_from_cumulative_distribution()

