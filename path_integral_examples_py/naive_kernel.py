import numpy as np
import typing as tp
from numba import njit, jit, int32
from os import path
import matplotlib.pyplot as plt
import time


# Using atomic units
m_e = 1.0;
h_bar = 1.0


# This calculates kernel from point source. Naive implementations: tries to
# calculate functional integrals directly with
@njit
def naive_kernel_for_point_1d(box_side_size: int,
                              box_side_len: float,
                              num_walkers: int,
                              num_samples: int,
                              step_size: float,
                              init_wf: np.ndarray,
                              system: bool) -> np.ndarray:
    dim = 1
    tau = 1.0
    a = box_side_len / 2.0

    box_dims=(box_side_size,)
    box_lower_corner=np.array((-a,))
    box_upper_corner=np.array((a,))
    assert init_wf.shape == box_dims
    #assert init_wf.dtype == np.complex_
    
    # Cumulative distribution for drawing random samples
    cdf = np.cumsum(np.abs(init_wf)**2) * (box_side_len/box_dims[0])

    wf = init_wf.copy()
    walker = 0

    while True:
        r_crd, _ = draw_sample_from_cumulative_distribution(box_lower_corner,
                                                        box_upper_corner, cdf)
        r: np.ndarray = np.zeros((num_samples, dim))  # wave function initially at center
        r[:] = r_crd
        step = (np.random.rand(num_samples, dim) - 0.5) * step_size
        r_old: np.ndarray = np.zeros(dim)
        for sample in range(num_samples):
            r[sample, :] = r_old + step[sample, :]
            r_old = r[sample, :]

            # Kill walker if it is outside the box.
            if not ((r_old >= box_lower_corner).all()
                    and (r_old <= box_upper_corner).all()):
                break
        else:
            action = S_1d(r, num_samples, system)
            mult = np.exp(1j * action)  # / h_bar
            ids = position_into_dens_index_1d(r[-1, 0],
                                              box_lower_corner[0],
                                              box_upper_corner[0],
                                              box_dims[0])
            assert(ids is not None)
            wf[int32(ids)] += mult

            walker += 1
            if walker >= num_walkers:
                break

    # normalize
    norm = (np.abs(wf)**2).sum() * box_side_len / box_side_size
    print(norm)
    wf *= 1/np.sqrt(norm)

    return wf

@njit
def L(r: np.ndarray, r_dot: np.ndarray) -> np.float_:
    return 0.5 * m_e * (r_dot @ r_dot) - 0  # V = 0@njit

@njit
def L_1d(r: float, r_dot: float, sys: bool) -> float:
    # omega = 1.0  # frequency of harmonic oscillator
    if sys:
        V = 0  # particle in  a box
    else:
        V = 0.5 * r**2  # Harmonic 1/2 * m_e * omega**2 * x**2
    return 0.5 * m_e * (r_dot * r_dot) - V

@njit
def S(r_t: np.ndarray, num_samples: int) -> np.float_:
    diff = np.zeros((1,))
    lagrange = 0.0
    lagrange += L(r_t[0, np.newaxis], diff)
    for i in range(1,num_samples):
        diff = r_t[i] - r_t[i-1]
    return lagrange

@njit
def S_1d(r_t: np.ndarray, num_samples: int, sys: bool) -> float:
    action = L_1d(r_t[0,0], 0.0, sys)
    for i in range(1,num_samples):
        diff = r_t[i,0] - r_t[i-1,0]
        action += L_1d(r_t[i,0], diff, sys)
    return action

@njit
def position_into_dens_index_1d(r: float,
                                low: float,
                                upp: float,
                                nx: int) \
        -> tp.Union[int, None]:
    x_i = int((r - low) * nx / (upp - low))

    if 0 <= x_i and x_i < nx:
        return x_i
    else:
        return None

@njit
def draw_sample_from_cumulative_distribution(x_start, x_end, cdf: np.ndarray)\
        -> tp.Tuple[float, int]:
    """ Get inverse of array (array is thought as a function). Assuming linear
    x axis.
    TODO?: I could increase performace by precaluclating inversed array.
    """
    assert (np.diff(cdf) >= 0).all()  # arr must be sorted
    assert len(cdf.shape) == 1  # 1d vector

    range_x = x_end - x_start
    range_y = cdf[-1] - cdf[0]

    id_y = np.searchsorted(cdf, cdf[0] + np.random.rand() * range_y )
    inverse_x = x_start + float(id_y)/len(cdf) * range_x
    return inverse_x, id_y


# ------------ Calling and plotting ---------------


def particle_in_a_box(x, L: float, n: int):
    k = n * np.pi / L
    A = np.sqrt(2 / L)
    wf = A * np.sin(k * (x+L/2)).astype(np.complex_)
    dens = np.abs(wf)**2
    wf *= np.sqrt(1/dens.sum() * (len(x)/L))
    return wf

def harmonic_oscillator(x, L, omega: float=1.0, n: int=1):
    wf = ((omega/np.pi)**(1/4) * np.exp(-omega * x**2 / (2))).astype(np.complex_)
    dens = np.abs(wf)**2
    wf *= np.sqrt(1/dens.sum()  * (len(x)/L))
    return wf

def run_particle_in_a_box(load_data=True):

    box_side_size = 65
    box_side_len = L = 2.0
    num_walkers = 1000000
    num_samples = 10000  # do not increase much or performance decreases
    step_size = 0.06  # do not increase much or performance decreases
    pib_n = 1  # particle in a box exitation
    s = "box walkers {}, samples {}, step {}, n {}".format(num_walkers,
                                                    num_samples,
                                                    step_size, 
                                                    pib_n,
                                                    np.random.randint(100))
    print(s)
    
    x = np.linspace(-1, 1, box_side_size)
    wf_pib = particle_in_a_box(x, box_side_len, pib_n)
    
    # if pre-calculated data already exists, use that
    if load_data and path.isfile(s+".npy"):
        wf_pi = np.load(s+".npy")
    else:

        elapsed = []
        start_time = time.time()
        wf_pi = naive_kernel_for_point_1d(box_side_size,
                                        box_side_len,
                                        num_walkers,
                                        num_samples,
                                        step_size,
                                        wf_pib,
                                        True)  # True means box
        end_time = time.time()
        elapsed.append(end_time - start_time)

        print("\n1d random walker")
        print("    elapsed:", elapsed)
        np.save(s, wf_pi)


    fig, ax1 = plt.subplots(1,1, figsize=(5,4))
    ax1.plot(x, np.abs(wf_pi)**2, label="monte carlo")
    ax1.plot(x, np.abs(wf_pib)**2, label="analytical")
    ax1.set_xlabel("x")
    ax1.set_ylabel("P")
    #fig.suptitle(s)
    ax1.legend()
    fig.savefig(s +".pdf")
    fig.savefig("images_in_slides/MC_box.pdf")
    
    #ax1.plot(x, (0.5+0.5*np.cos(x*np.pi))*np.mean(arr[30:35]), label="cos x")
    
    
def run_harmonic_oscillator(load_data=True):
    
    Hervanta_constant = 4.0

    box_side_size = 65
    box_side_len = L = 5.0
    num_walkers = 10000000000
    num_samples = 100   # do not increase much or performance decreases
    step_size = 0.6  # do not increase much or performance decreases
    n = 1  # particle in a box exitation
    s = "harmonic walkers {}, samples {}, step {}, n {}".format(
                                                    num_walkers,
                                                    num_samples,
                                                    step_size, 
                                                    n,
                                                    np.random.randint(100))
    print(s)
    
    x = np.linspace(-box_side_len/2, box_side_len/2, box_side_size)
    wf_pib = harmonic_oscillator(x, L, omega=1.0*Hervanta_constant, n=n)
    
    # if pre-calculated data already exists, use that
    if load_data and path.isfile(s+".npy"):
        wf_pi = np.load(s+".npy")
    else:
        #wf_pib = particle_in_a_box(x, box_side_len, n)

        elapsed = []
        start_time = time.time()
        wf_pi = naive_kernel_for_point_1d(box_side_size,
                                        box_side_len,
                                        num_walkers,
                                        num_samples,
                                        step_size,
                                        wf_pib,
                                        False)  # False means harmonic
        end_time = time.time()
        elapsed.append(end_time - start_time)

        print("\n1d random walker")
        print("    elapsed:", elapsed)
        np.save(s, wf_pi)


    fig, ax1 = plt.subplots(1,1, figsize=(5,4))
    ax1.plot(x, np.abs(wf_pi)**2, label="monte carlo")
    ax1.plot(x, np.abs(wf_pib)**2, label="analytical")
    ax1.set_xlim([-1.5,1.5])
    ax1.set_xlabel("x")
    ax1.set_ylabel("P")
    #fig.suptitle(s)
    ax1.legend()
    fig.savefig(s +".pdf")
    fig.savefig("images_in_slides/MC_harmonic.pdf")
    
    #ax1.plot(x, (0.5+0.5*np.cos(x*np.pi))*np.mean(arr[30:35]), label="cos x")
    


if __name__ == "__main__":
    run_particle_in_a_box()
    run_harmonic_oscillator()
    #compare_the_two()
    
    plt.show()
