extern crate path_integral_examples;
use path_integral_examples::rnd_walker_rs as wa;

use ndarray as nd;
#[allow(unused_imports)]
use nd::{Array, Array1, Array2, Array3, Axis, Zip};

#[allow(unused_imports)]
use std::io::stdin;
#[allow(unused_imports)]
use failure::Error;


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn run_main() {
        println!("#\n#\n#\n#\n#\n#\n#\n#\n\n");
        //wa::playground();
        test_rnd_walker_3d();
        //stdin().read_line(&mut String::new()).expect("Could not read input");
        //test_position_into_dens_index();
        println!("\n\n#\n#\n#\n#\n#\n#\n#\n#\n");
    }


    #[test]
    fn test_basic() {
        let num = wa::addf_rs(1.0, 2.0).unwrap();
        //_println!("Why on earth does this not find the crate?");
        //_println!("Oh, I must declare both 'cdylib' and 'lib' in cargo.toml");
        assert_eq!(num, 3.0);
    }

    #[test]
    fn test_rnd_walker_3d() {
        let box_side_size = 65;
        let box_side_len = 2.0;
        let num_walkers = 10;
        let num_samples = 1000;
        let interval = 100;
        let step_size = 0.001;

        let dens = wa::rnd_walker_3d_rs(box_side_size,
                                        box_side_len,
                                        num_walkers,
                                        num_samples,
                                        interval,
                                        step_size).unwrap();

        assert!(dens.sum() <= 1.000001, dens.sum());
    }

    #[test]
    fn test_position_into_dens_index_3d() {
        let box_side_size = 65;
        let box_side_len = 2.0;


        let rs: Array2<f64> = nd::arr2(
            &[[0.0, 0.0, 0.0],
                [-1.0, -1.0, -1.0],
                [ 0.0, -0.5, -1.0],
                [ 0.9999,  0.9999,  0.0],
                [ 0.0, -2.0,  0.0]]);
        let is: Array2<usize> = nd::arr2(
            &[[32, 32, 32],
                [0, 0, 0],
                [32, 16, 0],
                [64, 64, 32],
                [1000, 1000, 1000]]);  // out of bounds
        let box_dims = (box_side_size, box_side_size, box_side_size);
        let a = box_side_len / 2.0;
        let box_lower_corner = nd::array![-a, -a, -a];
        let box_upper_corner = nd::array![a, a, a];

        for (r, i) in (rs.axis_iter(Axis(0)))
                .zip(is.axis_iter(Axis(0))) {
            match wa::position_into_dens_index_3d(&r.to_owned(),
                                               &box_lower_corner,
                                               &box_upper_corner,
                                               box_dims) {
                Some((x, y, z)) => {
                    //dbg!((x, y, z));
                    // dbg!(
                    assert!((x as isize - i[0] as isize) == 0
                            && (y as isize - i[1] as isize) == 0
                            && (z as isize - i[2] as isize) == 0
                    );
                },
                None => {
                    //dbg!("crap");
                }
            }
        }
    }
}
