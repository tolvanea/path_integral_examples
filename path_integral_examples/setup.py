import sys

from setuptools import setup
from setuptools.command.test import test as TestCommand
#from setuptools_rust import RustExtension

try:
    from setuptools_rust import RustExtension#, Binding
except ImportError:
    import subprocess
    
    command = [sys.executable, "-m", "pip", "install", "setuptools-rust"]
    
    msg = "\n".join(
        ["\n\n| RustExtension not installed. To install it, please run command:\n| ",
         "| " + " ".join(command),
         "|\n| Also remeber that if you use have anaconda, you need to install do",
         "| installation there. Also, I guess you need to install rust nightly",
         "| compiler by hand. Oh and you need also blas libraries:"
         "| sudo apt-get install libopenblas-base libopenblas-dev"
         "| \n"])
    raise ImportError(msg)


class PyTest(TestCommand):
    user_options = []

    def run(self):
        self.run_command("test_rust")

        import subprocess

        errno = subprocess.call(["pytest", "tests"])
        raise SystemExit(errno)


def get_py_version_cfgs():
    # For now each Cfg Py_3_X flag is interpreted as "at least 3.X"
    version = sys.version_info[0:2]

    if version[0] == 2:
        return ["--cfg=Py_2"]

    py3_min = 5
    out_cfg = []
    for minor in range(py3_min, version[1] + 1):
        out_cfg.append("--cfg=Py_3_%d" % minor)

    return out_cfg


install_requires = []
tests_require = install_requires + ["pytest", "pytest-benchmark"]

setup(
    name="path_integral_examples",
    version="0.1.0",
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Programming Language :: Python",
        "Programming Language :: Rust",
        "Operating System :: POSIX",
        "Operating System :: Linux :: Linux",
    ],
    packages=["path_integral_examples"],
    rust_extensions=[
        RustExtension(
            "path_integral_examples.rnd_walker", "Cargo.toml",
            rustc_flags=get_py_version_cfgs()
                .extend(["--release", "-C", "target-cpu=native"])
        ),
    ],
    install_requires=install_requires,
    tests_require=tests_require,
    include_package_data=True,
    zip_safe=False,
    cmdclass=dict(test=PyTest),
)
