//! Implementations of python-side rnd_walker wrapper

use ndarray as nd;
#[allow(unused_imports)]
use nd::{Array, Array1, Array2, Array3, Axis, Zip};
use rand::distributions::Uniform;
use ndarray_rand::RandomExt;
#[allow(unused_imports)]
use failure::Error;

type Point = Array1<f64>;

pub fn addf_rs(a: f64, b: f64) -> Result<f64, Error>{
    if ! (a.is_nan() || b.is_nan()) {
        return Ok(a + b);
    }
    else{
        return Err(failure::err_msg("This didn't work out"));
    }
}

#[allow(dead_code)]
pub fn playground() {
    // TODO method to find all indices that are smaller than zero
    // Something like:
    // Zip::from(&mut a).and(&b).and(&c).apply(|a, &b, &c| {
    //     *a = b + c;
    // });
    // No! Use this:
    //a.mapv(|a| a > 0.5)
    let r1: Array1<f64> = nd::arr1(&[0.1, 0.2, 0.3]);
    let r2: Array1<f64> = nd::arr1(&[1.0, 2.0, 3.0]);
    let step: Array1<f64> = Array::random(3, Uniform::new(0., 1.0));
    let r3 = &r1 + &r2;
    let _r4 = &r1 + &step;

    dbg!(r3);

}

pub fn rnd_walker_1d_rs(box_side_size: usize,
                        box_side_len: f64,
                        num_walkers: usize,
                        num_samples: usize,
                        interval: usize,
                        step_size: f64)
                        -> Result<nd::Array1<f64>, Error>
{
    let dim = 1;
    let box_dims = (box_side_size,);
    let a = box_side_len / 2.0;
    let box_lower_corner = nd::array![-a];
    let box_upper_corner = nd::array![a];

    let mut dens_int = Array1::<u32>::zeros(box_dims);

    for _walker in 0..num_walkers {
        let mut r: Array1<f64> = Array::zeros((1,));
        for _i in 0..num_samples {
            let steps = Array::random((interval, dim),
                                      Uniform::new(-step_size, step_size));
            for step in steps.axis_iter(Axis(0)) {
                r += &step;
            }
            match position_into_dens_index_1d(&r,
                                              &box_lower_corner,
                                              &box_upper_corner,
                                              box_dims) {
                Some((x,)) => {dens_int[[x]] += 1;},
                None => ()
            }
        }
    }
    // normalize
    let count = (num_walkers*num_samples) as f64;
    let dens = dens_int.mapv(|a| a as f64 / count);

    return Ok(dens);

}

pub fn rnd_walker_2d_rs(box_side_size: usize,
                        box_side_len: f64,
                        num_walkers: usize,
                        num_samples: usize,
                        interval: usize,
                        step_size: f64)
                        -> Result<nd::Array2<f64>, Error>
{
    let dim = 2;
    let box_dims = (box_side_size, box_side_size);
    let a = box_side_len / 2.0;
    let box_lower_corner = nd::array![-a, -a];
    let box_upper_corner = nd::array![a, a];

    let mut dens_int = Array2::<u32>::zeros(box_dims);

    for _walker in 0..num_walkers {
        let mut r: Array1<f64> = nd::arr1(&[0.0, 0.0]);
        for _i in 0..num_samples {
            let steps = Array::random((interval, dim),
                                      Uniform::new(-step_size, step_size));
            for step in steps.axis_iter(Axis(0)) {
                r += &step;
            }
            match position_into_dens_index_2d(&r,
                                              &box_lower_corner,
                                              &box_upper_corner,
                                              box_dims) {
                Some((x, y)) => {dens_int[[x, y]] += 1;},
                None => ()
            }
        }
    }
    // normalize
    let count = (num_walkers*num_samples) as f64;
    let dens = dens_int.mapv(|a| a as f64 / count);

    return Ok(dens);

}


pub fn rnd_walker_3d_rs(box_side_size: usize,
                        box_side_len: f64,
                        num_walkers: usize,
                        num_samples: usize,
                        interval: usize,
                        step_size: f64)
                        -> Result<nd::Array3<f64>, Error>
{

    let box_dims = (box_side_size, box_side_size, box_side_size);
    let a = box_side_len / 2.0;
    let box_lower_corner = nd::array![-a, -a, -a];
    let box_upper_corner = nd::array![a, a, a];

    let mut dens_int = Array3::<u32>::zeros(box_dims);

    for _walker in 0..num_walkers {
        let mut r: Array1<f64> = nd::arr1(&[0.0, 0.0, 0.0]);
        for _i in 0..num_samples {
            let steps = Array::random((interval, 3), Uniform::new(-step_size,
                                                                  step_size));
            for step in steps.axis_iter(Axis(0)) {
                r += &step;
            }
            match position_into_dens_index_3d(&r,
                                              &box_lower_corner,
                                              &box_upper_corner,
                                              box_dims) {
                Some((x, y, z)) => {dens_int[[x, y, z]] += 1;},
                None => ()
            }
        }
    }
    // normalize
    let count = (num_walkers*num_samples) as f64;
    let dens = dens_int.mapv(|a| a as f64 / count);

    return Ok(dens);

}


pub fn position_into_dens_index_1d(r: &Point,
                                   low: &Point,
                                   upp: &Point,
                                   (nx,): (usize,))
                                   -> Option<((usize,))> {

    let x_i = ((r[0] - low[0]) * (nx as f64) / (upp[0] - low[0])) as isize;

if 0 <= x_i && x_i < nx as isize{
        return Some((x_i as usize,));
    }
    else {
        return None;
    }
}


pub fn position_into_dens_index_2d(r: &Point,
                                   low: &Point,
                                   upp: &Point,
                                   (nx, ny): (usize, usize))
                                   -> Option<(usize, usize)> {

    let x_i = ((r[0] - low[0]) * (nx as f64) / (upp[0] - low[0])) as isize;
    let y_i = ((r[1] - low[1]) * (ny as f64) / (upp[1] - low[1])) as isize;

    if (0 <= x_i && x_i < nx as isize)
            && (0 <= y_i && y_i < ny as isize){
        return Some((x_i as usize, y_i as usize));
    }
    else {
        return None;
    }
}


pub fn position_into_dens_index_3d(r: &Point,
                                   low: &Point,
                                   upp: &Point,
                                   (nx, ny, nz): (usize, usize, usize))
                                   -> Option<(usize, usize, usize)> {

    let x_i = ((r[0] - low[0]) * (nx as f64) / (upp[0] - low[0])) as isize;
    let y_i = ((r[1] - low[1]) * (ny as f64) / (upp[1] - low[1])) as isize;
    let z_i = ((r[2] - low[2]) * (nz as f64) / (upp[2] - low[2])) as isize;

    if (0 <= x_i && x_i < nx as isize)
            && (0 <= y_i && y_i < ny as isize)
            && (0 <= z_i && z_i < nz as isize){
        return Some((x_i as usize, y_i as usize, z_i as usize));
    }
    else {
        return None;
    }
}