#![feature(specialization)]

pub mod rnd_walker_rs;

//#[macro_use]
extern crate pyo3;
use pyo3 as py;
use py::prelude as pp;
use pp::{pymodinit};

use ndarray_linalg::solve::Inverse;

use numpy::{IntoPyArray, PyArray1, PyArray2, PyArray3};




/// This module is a python module implemented in Rust.
#[pymodinit]
fn rnd_walker(_py: pp::Python, m: &pp::PyModule) -> pp::PyResult<()> {
    use rnd_walker_rs as wa;
    use py::exceptions::ValueError;

    /// Adds two floats together
    #[pyfn(m, "addf")]
    fn addf(a: f64, b: f64) -> pp::PyResult<f64> {
        //py::exceptions::ValueError
        match wa::addf_rs(a, b){
            Ok(v) => Ok(v),
            Err(error) => Err(ValueError::py_err(error.to_string()))
        }
    }

    /// TESTING: Takes inverse of array
    #[pyfn(m, "inv")]
    fn inv(py: pp::Python, x: &PyArray2<f64>)
        -> pp::PyResult<pp::Py<PyArray2<f64>>>
    {
        match x.as_array().inv() {
            Ok(v) => Ok(v.into_pyarray(py).to_owned()),
            Err(_e) => Err(ValueError::py_err("Shit happened."))//format!("{}", e)))
        }
    }


    /// Calculates density of random 2d walker.
    /// Box is centered at 0, so that x axis ranges from -L/2 to L/2 where
    /// L is 'dens_box_side_len'.
    #[pyfn(m, "rnd_walker_1d")]
    fn rnd_walker_1d(py: pp::Python,
                     box_side_size: usize,
                     box_side_len: f64,
                     num_walkers: usize,
                     num_samples: usize,
                     interval: usize,
                     step_size: f64)
        -> pp::PyResult<pp::Py<PyArray1<f64>>>
    {
        match wa::rnd_walker_1d_rs(box_side_size,
                                   box_side_len,
                                   num_walkers,
                                   num_samples,
                                   interval,
                                   step_size) {
            Ok(arr) => Ok(arr.into_pyarray(py).to_owned()),
            Err(_err) => Err(ValueError::py_err(
                "Something went wrong. TODO figure rust error types and then \n\
                 return other python errors than ValueError."))
        }
    }

    /// Calculates density of random 2d walker.
    /// Box is centered at 0, so that x, y axis ranges from -L/2 to L/2 where
    /// L is 'dens_box_side_len'.
    #[pyfn(m, "rnd_walker_2d")]
    fn rnd_walker_2d(py: pp::Python,
                     box_side_size: usize,
                     box_side_len: f64,
                     num_walkers: usize,
                     num_samples: usize,
                     interval: usize,
                     step_size: f64)
        -> pp::PyResult<pp::Py<PyArray2<f64>>>
    {
        match wa::rnd_walker_2d_rs(box_side_size,
                                   box_side_len,
                                   num_walkers,
                                   num_samples,
                                   interval,
                                   step_size) {
            Ok(arr) => Ok(arr.into_pyarray(py).to_owned()),
            Err(_err) => Err(ValueError::py_err(
                "Something went wrong. TODO figure rust error types and then \n\
                 return other python errors than ValueError."))
        }
    }

    /// Calculates density of random 3d walker.
    /// Box is centered at 0, so that x, y, z axis ranges from -L/2 to L/2 where
    /// L is 'dens_box_side_len'.
    #[pyfn(m, "rnd_walker_3d")]
    fn rnd_walker_3d(py: pp::Python,
                     box_side_size: usize,
                     box_side_len: f64,
                     num_walkers: usize,
                     num_samples: usize,
                     interval: usize,
                     step_size: f64)
        -> pp::PyResult<pp::Py<PyArray3<f64>>>
    {
        match wa::rnd_walker_3d_rs(box_side_size,
                                   box_side_len,
                                   num_walkers,
                                   num_samples,
                                   interval,
                                   step_size) {
            Ok(arr) => Ok(arr.into_pyarray(py).to_owned()),
            Err(_err) => Err(ValueError::py_err(
                "Something went wrong. TODO figure rust error types and then \n\
                 return other python errors than ValueError."))
        }
    }

    Ok(())
}


#[cfg(test)]
mod tests {
    #[test]
    fn internal() {
        assert_eq!(1, 1, "Tests will be on python side.");
    }
}