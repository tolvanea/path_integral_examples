\section{Path integral formalism of quantum mechanics}

\begin{frame}
    \frametitle{Overview}
    \tableofcontents[currentsection]
\end{frame}


\begin{frame}
    \frametitle{Path integral formalism of quantum mechanics}
     Instead of using Schr\"odinger equation, quantum mechanics can also be formulated with \textit{path integrals}
    \begin{columns}[T,onlytextwidth]
        \begin{column}{0.45\textwidth}
            \begin{block}{Schr\"odinger equation}
                \begin{flalign*}
                    i \hbar \frac{d}{d t}\Psi
                    =
                    \frac{-\hbar^2}{2m}\nabla^2 \Psi + V \Psi
                \end{flalign*}
                \begin{itemize}
                    \item Second order differential equation
                    \item Actually, it is a diffusion equation with imaginary diffusion constant
                \end{itemize}
            \end{block}

        \end{column}
        \begin{column}{0.45\textwidth}
            \begin{block}{Path integrals}
                \begin{flalign*}
                    & \int e^{\frac{i}{\hbar}S[x(t)]} \mathcal{D}x(t)&
                \end{flalign*}
                \begin{itemize}
                    \item Total contribution of infinitely many trajectories
                    \item[+] More intuitive
                    \item[+] Useful with some tricky problems
                    \item[--] Computationally heavy
                \end{itemize}
            \end{block}
        \end{column}

    \end{columns}
\end{frame}
% - The one can be derived from another



\begin{frame}
    \frametitle{Double slit experiment}
    %Schr\"odinger equation

    \begin{columns}[T,onlytextwidth]
        \begin{column}{0.68\textwidth}
            \begin{block}{Double slit}
                \begin{itemize}
                    \item Phase difference creates interference pattern
                    \item Sum two \textit{probability amplitudes} together
                    \vspace*{-5mm}%
                    \begin{flalign*}
                        &K = \phi_1 + \phi_2&\\
                        &P = |K|^2= |\phi_1|^2 + |\phi_2|^2 + 2\mathrm{Re}(\phi_1^*\phi_2)&
                    \end{flalign*}
                \end{itemize}
            \end{block}
            \vspace*{-6mm}
            \begin{block}{Many slits}
                \begin{itemize}
                    \item Total probability amplitude is a sum
                        \vspace*{-2mm}
                        \begin{flalign*}
                            &K = \sum_i^N \phi_i&
                        \end{flalign*}
                        \vspace*{-2mm}
                \end{itemize}
            \end{block}
        \end{column}
        \begin{column}{0.40\textwidth}
            \begin{figure}
                \includegraphics[width=0.99\linewidth]{pics/slits_double.png}
            \end{figure}
            \begin{figure}
                \includegraphics[width=0.99\linewidth]{pics/slits_n.png}
            \end{figure}
        \end{column}

    \end{columns}

    \let\thefootnote\relax\footnote{\scriptsize
        Images from~\cite{olofahlenGPUImplementationFeynman}}
\end{frame}
% - This is a bit like a photon traveling through slits: Electric field goes circle in complex plane. Photon phase.
% - superposition







\begin{frame}
    \frametitle{Forming arbitrary path}
    \vspace*{-5mm}
    \begin{columns}[T,onlytextwidth]
        \begin{column}{0.35\textwidth}
            \begin{figure}
                \includegraphics[width=0.9\linewidth]{pics/transparent_square.png}
            \end{figure}
        \end{column}
        \begin{column}{0.27\textwidth}
            \begin{figure}
                \includegraphics[width=0.9\linewidth]{pics/slits_n_serial.png}
            \end{figure}
        \end{column}
        \begin{column}{0.27\textwidth}
            \begin{figure}
                \includegraphics[width=0.9\linewidth]{pics/slits_serial.png}
            \end{figure}
        \end{column}
    \end{columns}
    \vspace*{-20mm}
    \begin{block}{Multiple serial layers}
        \begin{itemize}
            \item Take into account all possible combinations of slit passages
            \item Path $x(t)$ is function of time
            \item Each path contribute the same amount
            \item Total probability amplitude from start point $a=(x_a,t_a)$ to end point $b=(x_b,t_b)$ is
                \begin{flalign*}
                    &K(a,b) = \sum_{\text{all paths } x(t)} \phi[x(t)] \;\; \eqover \int_a^b \phi [x(t)] \mathcal{D}x(t) &
                \end{flalign*}
        \end{itemize}
    \end{block}
    \vspace*{-5mm}
    \let\thefootnote\relax\footnote{\scriptsize
        Image from~\cite{feynmanQuantumMechanicsPath2010}\cite{christopherlepenikPathIntegralFormalism}}
\end{frame}

\begin{frame}
    \frametitle{Example of 1d path}
    For example, path $x(t)$ can be following:
    \begin{figure}
        \includegraphics[width=0.6\linewidth]{pics/1d_path.png}
    \end{figure}
    \let\thefootnote\relax\footnote{\scriptsize
        Image from~\cite{feynmanQuantumMechanicsPath2010}}
\end{frame}
% Particle can also travel backwards between layers

\begin{frame}
    \frametitle{Action}

    \begin{block}{Action of trajectory}
        An \textit{action} $S$ can be assigned to path $x(t)$ by formula
        \begin{flalign*}
            &\qquad S[x(t)] = \int_{t_a}^{t_b} L(x, \dot{x}, t) \dd t&\\
            &\qquad L(x, \dot{x}, t) = \underbrace{\frac{1}{2} m \dot{x}^2}_{\text{kinetic}} - \underbrace{V(x,t)}_{\text{potential}}&
        \end{flalign*}
        where $L$ is Lagrangian, and square bracket notation $S[x(t)]$ means functional.
    \end{block}
    \vspace*{-5mm}
    \begin{columns}[c,onlytextwidth]
        \begin{column}{0.6\textwidth}
            Classical trajectory $x_c(t)$ is defined by \textit{principle of least action} (i.e. $\delta S = 0$)
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=0.99\linewidth]{pics/princible_of_minimal_action.png}
            \end{figure}
        \end{column}
    \end{columns}
    \vspace*{-5mm}
    \let\thefootnote\relax\footnote{\scriptsize
        Image from~\cite{riccardorattazziPathIntegralApproach}}
\end{frame}
% - Lets introduce, out of blue, the action
% - Using names "path" and "trajectory"
% - L is functional, it takes in a function and return one number
% - if astronaut throws a ball in outer space, it goes straight line. If potential is present, e.g. the ball is thrown at the orbit, then trajectory is not straight line.
% - Ota kuminauha mukaan! Näytä kuinka suoraa ei muuttaa mihinkään suuntaan siten että se enää lyhenisi.


\begin{frame}
    \frametitle{Fermat's princible}
    Side note / analogy: \\
    Light ray takes the path that can be traversed in the least time
    \begin{figure}
                \includegraphics[width=0.5\linewidth]{pics/snell-1.png}
    \end{figure}
    \let\thefootnote\relax\footnote{\scriptsize
        Image from \url{https://en.wikipedia.org/wiki/Snell's_law}}
\end{frame}


\begin{frame}
    \frametitle{Probability amplitude by function of action}
    \begin{block}{Determining propability amplitude $\phi$ for single path}
        \begin{itemize}
            \item Probability amplitude of random path $\phi[x(t)]$ is function of action $S[x(t)]$
            \item We want the classical path to dominate with small $\hbar$. So as $\hbar \to 0$, should $K \approx \phi[x_c(t)]$
            \item (We also want other nice properties for total amplitude $K$)
            %like that sequentially combining $K(a,b)$ and $K(b,c)$ gives $K(a,c)$
            \item Turns out there is such solution for probability amplitude of single path~\cite{riccardorattazziPathIntegralApproach}
                \begin{flalign*}
                    &\qquad \phi[x(t)] = \phi(S) = e^{\frac{i}{\hbar}S}&
                \end{flalign*}
            \item Note, oscillating phase $e^{\frac{i}{\hbar}S}$
        \end{itemize}
    \end{block}
\end{frame}
% We also want that sequentially combining $K(a,b)$ and $K(b,c)$ gives $K(a,c)$
% I dont tell you what this sequential means


\begin{frame}
    \frametitle{Total probability amplitude}
    \begin{block}{Total probability amplitude}
        Now total probability amplitude becomes
        \begin{flalign*}
            \qquad K(a,b) &= \int_a^b \phi[x(t)] \mathcal{D}x(t)&\\
            &=\int_a^b e^{\frac{i}{\hbar}S[x(t)]} \mathcal{D}x(t)&
        \end{flalign*}
        Just to recall, action $S$ is
        \begin{flalign*}
            &\qquad S[x(t)] = \int_{t_a}^{t_b} L(x, \dot{x}, t) \dd t&\\
            &\qquad L(x, \dot{x}, t) = \frac{1}{2} m \dot{x}^2 - V(x,t)&
        \end{flalign*}
%        \begin{itemize}
%        \end{itemize}
    \end{block}
\end{frame}
% -Does this seem similar?



\begin{frame}
    \frametitle{Figure of 1d path}
    Paths close to classical paths contribute the most, paths far away cancel each other out.
    \begin{figure}
        \includegraphics[width=0.6\linewidth]{pics/least_action_principle.png}
    \end{figure}

        \let\thefootnote\relax\footnote{\scriptsize
        Image from \url{https://en.wikipedia.org/wiki/Principle_of_least_action}}
\end{frame}

\section{Toy program}

\begin{frame}
    \frametitle{Overview}
    \tableofcontents[currentsection]
\end{frame}

\begin{frame}
    \frametitle{Time to put theory to a test!}
    I played around with two different programming languages:\\ Python and Rust
    \begin{columns}[T,onlytextwidth]
        \begin{column}{0.5\textwidth}
            \begin{block}{Python + Numba}
                \begin{itemize}
                    \item Numba is a Python library that does JIT-compilation\\
                    \item[+] C-level performance, (100x speedup compared to regular Python)
                    \item[+] Good library support with Numpy
                    \item[--] Code is restricted to basic Python
                \end{itemize}
            \end{block}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{block}{Rust}
                \begin{itemize}
                    \item Rust is a modern low-level language similar to C/C++
                    \item[+] C-level performance\\~\\~
                    \item[--] Poor library support (mainly C-library bindings)
                    \item[+] High level language constructs
                \end{itemize}
            \end{block}
        \end{column}
    \end{columns}
    Clear winner: \textbf{Python+Numba} for this project written by mentality \textit{throw-something-together-quick}.
\end{frame}
% Why rust?
%       Writing wrong code is really hard, so not much time is spend debugging.

\begin{frame}[fragile] % Need to use the fragile option when verbatim is used in the slide
    \frametitle{Converting Python code to Numba}
    In the best case, converting already existing code to Numba is quite easy.
    \begin{columns}[T,onlytextwidth]
        \begin{column}{0.5\textwidth}
            \begin{block}{Pure python}
                \begin{verbatim}
def my_sum(a,b):
    return a+b\end{verbatim}
            \end{block}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{block}{Numba}
                \begin{verbatim}
from numba import njit
@njit
def my_sum(a,b):
    return a+b\end{verbatim}
            \end{block}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Results}
    \begin{columns}[T,onlytextwidth]
        \begin{column}{0.45\textwidth}
            \begin{block}{Particle in a box}
                \begin{figure}
                    \includegraphics[width=0.99\linewidth]{pics/MC_box.pdf}
                \end{figure}
            \end{block}
        \end{column}
        \begin{column}{0.45\textwidth}
            \begin{block}{Harmonic oscillator}
                \begin{figure}
                    \includegraphics[width=0.99\linewidth]{pics/MC_harmonic.pdf}
                \end{figure}
                Note: Angular frequency $\omega$ is scaled by Hervanta constant $H_c=4$.
            \end{block}
        \end{column}
    \end{columns}
    Code: \cite{CodeGitlabHttps}
\end{frame}
% Kasvatin N:ää pari nollaa, pari nollaa lisää, ja vielä pari nollaa
% There's a lot of ways to make this better
