import numpy as np
import path_integral_examples as PI_rs
import path_integral_examples_py as PI_py
import matplotlib.pyplot as plt
import time

def main():
    #plot_rnd_walker_1d()
    plot_rnd_walker_2d()
    plot_rnd_walker_3d()
    #PI_py.plot_it()
    plt.show()

def test_sum():
    print("1+2=", PI.rnd_walker.addf(1.0, 2.0))
    

def test_inv():
    x = np.array([[1, 0],
                  [0, 2],], dtype=np.float64)
    y1 = PI_rs.inv(x)
    y2 = np.linalg.inv(x)
    print("y1, y2\n", y1, "\n", y2)
    np.testing.assert_array_almost_equal(y1, y2)


def plot_rnd_walker_1d():
    box_side_size = 65
    box_side_len = 2.0
    num_walkers = 1000
    num_samples = 1000
    interval = 100
    step_size = 0.01
    
    mods = [PI_rs, PI_py]
    names = ["rust, python"]
    for i in range(2):
        elapsed = []
        for mod in mods:
            start_time = time.time()
            arr = mod.rnd_walker_1d(box_side_size,
                                box_side_len,
                                num_walkers,
                                num_samples,
                                interval,
                                step_size)
            end_time = time.time()
            elapsed.append(end_time - start_time)
        
    print("\n2d random walker")
    print("    elapsed:", elapsed)
        
        
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2,2, figsize=(10,10))
    ax1.plot(np.linspace(-1, 1, 65), arr[:], label="x")
    ax1.legend()

    
def plot_rnd_walker_2d():
    box_side_size = 65
    box_side_len = 2.0
    num_walkers = 1000
    num_samples = 1000
    interval = 100
    step_size = 0.01
    
    mods = [PI_rs, PI_py]
    names = ["rust, python"]
    for i in range(2):
        elapsed = []
        for mod in mods:
            start_time = time.time()
            arr = mod.rnd_walker_2d(box_side_size,
                                box_side_len,
                                num_walkers,
                                num_samples,
                                interval,
                                step_size)
            end_time = time.time()
            elapsed.append(end_time - start_time)
        
    print("\n2d random walker")
    print("    elapsed:", elapsed)
        
        
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2,2, figsize=(10,10))
    ax1.imshow(arr)
    ax4.plot(np.linspace(-1, 1, 65), arr[:,32], label="x")
    ax4.plot(np.linspace(-1, 1, 65), arr[32,:], label="y")
    ax4.legend()
    
    
def plot_rnd_walker_3d():
    box_side_size = 65
    box_side_len = 2.0
    num_walkers = 1000
    num_samples = 1000
    interval = 100
    step_size = 0.01
    
    mods = [PI_rs, PI_py]
    for i in range(2):
        elapsed = []
        for mod in mods:
            start_time = time.time()
            arr = mod.rnd_walker_3d(box_side_size,
                                box_side_len,
                                num_walkers,
                                num_samples,
                                interval,
                                step_size)
            end_time = time.time()
            elapsed.append(end_time - start_time)
            
    print("\n3d random walker")
    print("    elapsed:", elapsed)
        
        
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2,2, figsize=(10,10))
    ax1.imshow(arr.sum(axis=0))
    ax2.imshow(arr.sum(axis=1))
    ax3.imshow(arr.sum(axis=2))
    ax4.plot(np.linspace(-1, 1, 65), arr[:,32,32], label="x")
    ax4.plot(np.linspace(-1, 1, 65), arr[32,:,32], label="y")
    ax4.plot(np.linspace(-1, 1, 65), arr[32,32,:], label="z")
    ax4.legend()
    
if __name__ == "__main__":
    main()
