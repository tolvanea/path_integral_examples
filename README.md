# path_integral_examples

Demo programs calculating some basic quantum mechanical systems using path integral formalism.  These demos were made for presentation on TAU course "FYS-1550 Fysiikan seminaari". 

Note: This is not a polished product, rather "throw something together quick". Therefore there exist some trash e.g. deprecated testing scripts.

All code is under MIT -licende.

## Project structure
This project consist of two parts, which were compared by speed. They are equally efficient. (However, rust libraries were lacking function "np.searchsorted", so naive path integral was not implemented with it.)

* Python + Numba module ```path_integral_examples_py/```

    * Implements random walker and naive path integral
    
* Rust ```path_integral_examples/``` (optional)

    * Implements only random walker
    
    
## Usage 

* Plot random walker image
    
    * ```python3 plot_random_walkers.py```
    
* Plot 1d path integral calculation

    * ```python3 naive_kernel.py```
    
All other files are some testing 


## Rust installation on Linux

Todo: test it on other machine

Install rust compiler. Notice! Compiler is required to be nighlty version. (Tested with rustc 1.34.0-nightly.)
Run
```curl https://sh.rustup.rs -sSf | sh```
and follow installation wizard.

Install setuptools-rust
```/usr/bin/python3 -m pip install setuptools-rust```

Compile 'path_integral_examples' python package. (This is made with rust.)
```cd path_integral_examples```
```pip3 install . --verbose```
```cd ..```

Run plotting file (that calls compiled module)
```python3 plot_all.py```

## My other toy programs related to path integrals or Monte Carlo

* Path integral Monte Carlo program written on Ilkkä Kylänpää's Computationl physics course. (There's also efficient rust optimization.)
    * https://gitlab.com/tolvanea/fys-4096_compphys/-/tree/master/project_work_2/prob4
* Classical thermodynamical sampling in quantum dots:
    * https://gitlab.com/tolvanea/MetropolisMC
